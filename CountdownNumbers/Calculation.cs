﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownNumbers
{
    public class Calculation
    {
        int n;
        int used;
        private int result;
        string operation;

        public int UsedDigits
        {
            get { return used; }
        }

        public int Result
        {
            get { return result; }
        }

        public string Operation
        {
            get { return operation; }
        }

        public Calculation(int n)
        {
            this.n = n;
            this.used = 0;
            this.result = 0;
            this.operation = string.Empty;
        }

        public static Calculation Add(Calculation left, Calculation right)
        {
            Calculation result = new Calculation(left.n);
            result.used = left.used | right.used;
            result.result = left.result + right.result;
            result.operation = string.Format("({0}+{1})", left.operation, right.operation);

            return result;
        }

        public static Calculation Subtract(Calculation left, Calculation right)
        {
            Calculation result = new Calculation(left.n);
            result.used = left.used | right.used;
            result.result = left.result - right.result;
            result.operation = string.Format("({0}-{1})", left.operation, right.operation);

            return result;
        }

        public static Calculation Multiply(Calculation left, Calculation right)
        {
            Calculation result = new Calculation(left.n);
            result.used = left.used | right.used;
            result.result = left.result * right.result;
            result.operation = string.Format("({0}*{1})", left.operation, right.operation);

            return result;
        }

        public static Calculation Divide(Calculation left, Calculation right)
        {
            Calculation result = new Calculation(left.n);
            result.used = left.used | right.used;
            result.operation = string.Format("({0}/{1})", left.operation, right.operation);

            if (right.result == 0 || left.result % right.result != 0)
                result.result = 0;
            else
                result.result = left.result / right.result;

            return result;
        }

        public static bool ValidateUsedNumbers(int left, int right)
        {
            return (left & right) == 0;
        }

        public static Calculation[] InitializeCalculations(int[] numbers)
        {
            Calculation[] result = new Calculation[numbers.Length];

            for (int i = 0; i < numbers.Length; i++)
            {
                result[i] = new Calculation(numbers.Length);
                result[i].used = (1 << i);
                result[i].result = numbers[i];
                result[i].operation = numbers[i].ToString();
            }

            return result;
        }
    }
}
