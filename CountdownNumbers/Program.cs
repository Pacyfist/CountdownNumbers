﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Six Numbers:");

            int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();

            if (numbers.Length != 6)
            {
                Console.WriteLine("There should be 6 but there are {0} numbers!", numbers.Length);
                Console.ReadLine();
                return;
            }

            Dictionary<int, Dictionary<int, Calculation>> calculations_1 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (Calculation calculation in Calculation.InitializeCalculations(numbers))
            {
                if (!calculations_1.ContainsKey(calculation.Result))
                    calculations_1[calculation.Result] = new Dictionary<int, Calculation>();

                calculations_1[calculation.Result][calculation.UsedDigits] = calculation;
            }

            Console.WriteLine("Calculations Step 1 Finished");

            Dictionary<int, Dictionary<int, Calculation>> calculations_2 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (int i in calculations_1.Keys)
                foreach (int j in calculations_1.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_1[i];
                    Dictionary<int, Calculation> rights = calculations_1[j];

                    CalculateResults(calculations_2, lefts, rights);
                }

            Console.WriteLine("Calculations Step 2 Finished");

            Dictionary<int, Dictionary<int, Calculation>> calculations_3 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (int i in calculations_1.Keys)
                foreach (int j in calculations_2.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_1[i];
                    Dictionary<int, Calculation> rights = calculations_2[j];

                    CalculateResults(calculations_3, lefts, rights);
                }

            Console.WriteLine("Calculations Step 3 Finished");

            Dictionary<int, Dictionary<int, Calculation>> calculations_4 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (int i in calculations_1.Keys)
                foreach (int j in calculations_3.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_1[i];
                    Dictionary<int, Calculation> rights = calculations_3[j];

                    CalculateResults(calculations_4, lefts, rights);
                }
            foreach (int i in calculations_2.Keys)
                foreach (int j in calculations_2.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_2[i];
                    Dictionary<int, Calculation> rights = calculations_2[j];

                    CalculateResults(calculations_4, lefts, rights);
                }

            Console.WriteLine("Calculations Step 4 Finished");

            Dictionary<int, Dictionary<int, Calculation>> calculations_5 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (int i in calculations_1.Keys)
                foreach (int j in calculations_4.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_1[i];
                    Dictionary<int, Calculation> rights = calculations_4[j];

                    CalculateResults(calculations_5, lefts, rights);
                }
            foreach (int i in calculations_2.Keys)
                foreach (int j in calculations_3.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_2[i];
                    Dictionary<int, Calculation> rights = calculations_3[j];

                    CalculateResults(calculations_5, lefts, rights);
                }

            Console.WriteLine("Calculations Step 5 Finished");

            Dictionary<int, Dictionary<int, Calculation>> calculations_6 = new Dictionary<int, Dictionary<int, Calculation>>();
            foreach (int i in calculations_1.Keys)
                foreach (int j in calculations_5.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_1[i];
                    Dictionary<int, Calculation> rights = calculations_5[j];

                    CalculateResults(calculations_6, lefts, rights);
                }
            foreach (int i in calculations_2.Keys)
                foreach (int j in calculations_4.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_2[i];
                    Dictionary<int, Calculation> rights = calculations_4[j];

                    CalculateResults(calculations_6, lefts, rights);
                }
            foreach (int i in calculations_3.Keys)
                foreach (int j in calculations_3.Keys)
                {
                    Dictionary<int, Calculation> lefts = calculations_3[i];
                    Dictionary<int, Calculation> rights = calculations_3[j];

                    CalculateResults(calculations_6, lefts, rights);
                }

            Console.WriteLine("Calculations Step 6 Finished");

            Console.WriteLine("Search for:");

            int search = int.Parse(Console.ReadLine());

            if (calculations_1.ContainsKey(search))
                foreach (Calculation c in calculations_1[search].Values)
                    Console.WriteLine(c.Operation);

            if (calculations_1.ContainsKey(search))
                foreach (Calculation c in calculations_1[search].Values)
                    Console.WriteLine(c.Operation);

            if (calculations_3.ContainsKey(search))
                foreach (Calculation c in calculations_3[search].Values)
                    Console.WriteLine(c.Operation);

            if (calculations_4.ContainsKey(search))
                foreach (Calculation c in calculations_4[search].Values)
                    Console.WriteLine(c.Operation);

            if (calculations_5.ContainsKey(search))
                foreach (Calculation c in calculations_5[search].Values)
                    Console.WriteLine(c.Operation);

            if (calculations_6.ContainsKey(search))
                foreach (Calculation c in calculations_6[search].Values)
                    Console.WriteLine(c.Operation);

            Console.ReadLine();
        }

        private static void CalculateResults(Dictionary<int, Dictionary<int, Calculation>> results, Dictionary<int, Calculation> lefts, Dictionary<int, Calculation> rights)
        {
            foreach (Calculation left in lefts.Values)
                foreach (Calculation right in rights.Values)
                {
                    if (Calculation.ValidateUsedNumbers(left.UsedDigits, right.UsedDigits))
                    {
                        Calculation addition = Calculation.Add(left, right);

                        if (!results.ContainsKey(addition.Result))
                            results[addition.Result] = new Dictionary<int, Calculation>();

                        results[addition.Result][addition.UsedDigits] = addition;


                        Calculation subtraction = Calculation.Subtract(left, right);

                        if (subtraction.Result > 0)
                        {
                            if (!results.ContainsKey(subtraction.Result))
                                results[subtraction.Result] = new Dictionary<int, Calculation>();

                            results[subtraction.Result][subtraction.UsedDigits] = subtraction;
                        }

                        Calculation multiplication = Calculation.Multiply(left, right);

                        if (!results.ContainsKey(multiplication.Result))
                            results[multiplication.Result] = new Dictionary<int, Calculation>();

                        results[multiplication.Result][multiplication.UsedDigits] = multiplication;


                        Calculation division = Calculation.Divide(left, right);

                        if (division.Result > 0)
                        {
                            if (!results.ContainsKey(division.Result))
                                results[division.Result] = new Dictionary<int, Calculation>();

                            results[division.Result][division.UsedDigits] = division;
                        }
                    }
                }
        }
    }
}
